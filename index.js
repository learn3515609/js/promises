import fetch from "node-fetch";

const delay = (ms) => new Promise(resolve => setTimeout(resolve, ms));

const delayResolver = (value, ms) => new Promise(async (resolve) => {
  await delay(ms);
  resolve(value);
})

const errorResolver = (value) => new Promise((_, reject) => setTimeout(() => {reject('my-error')}, 1000));

const promisify = (fn) => (...args) => new Promise(resolve => fn(...args, resolve));

const p1 = delayResolver('first', 500);
const p2 = delayResolver('second', 1000);
const p3 = delayResolver('third', 1500);
const p4 = errorResolver(1000);

// #01 - all
Promise.all([p1, p2, p3]).then(data => console.log('#01 - all', data));

// #02 - race
Promise.race([p1, p2, p3]).then(data => console.log('#02 - race', data));

// #03 - allSettled
Promise.allSettled([p1, p2, p3, p4]).then(data => console.log('#03 - allSettled', data));

// #04 - promisify
const loadItemsCount = (param, callback) => {
  fetch(`https://api.placeholderjson.dev/todo/${param}`).then(data => data.json()).then(items => callback(items.length))
}
const promisifeLoadItems = promisify(loadItemsCount);
promisifeLoadItems(1).then(count => console.log('#04 - promisify', count));

// #05 - errors, finally
p3
  .then(data => {console.log('#05 - errors, finally - 1', data); return p3})
  .then(data => {console.log('#05 - errors, finally - 2', data); return p4})
  .then(data => {console.log('#05 - errors, finally - 3', data); return p3})
  .catch(error => {console.log('#05 - errors, finally error - 4', error); return 'after error'})
  .finally(() => {console.log('#05 - errors, finally finally - 5')})
  .then(data => {console.log('#05 - errors, finally - 6', data);})

// #06 - for await

for await(let res of [p1, p2, p3]) {
  console.log('#06 - for await', res)
}

// #07 - thenable

const thenable = {
  then: function(onFulfilled) {
    setTimeout(() => onFulfilled(42), 2000);
  }
};

thenable.then(res => console.log('#07 - thenable', res))